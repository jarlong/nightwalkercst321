﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneController : MonoBehaviour {

    public GameObject PhoneLight;
    public GameObject PhoneMap;
    public Material phoneOff;
    public GameObject phone;
    public Slider PhoneBattery;
    public GameObject phoneBatteryHolder;

    private bool lightToggle = true;
    private bool mapToggle = true;
    private float timer = 14400;
    private float timerDecreaseRate = 1;
	// Use this for initialization
	void Start () {
        phone.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {

        if (timer >= 0)
        {
            if(lightToggle && mapToggle)
            {
                timerDecreaseRate = 2.0f;
            }
            else if(lightToggle)
            {
                timerDecreaseRate = 1.5f;
            }
            else if(mapToggle)
            {
                timerDecreaseRate = 1.5f;
            }
            else
            {
                timerDecreaseRate = 1;
            }
            timer = timer - (1 * timerDecreaseRate);
            PhoneBattery.value = (timer/720);
            if (OVRInput.GetDown(OVRInput.Button.One))
            {
                lightToggle = !lightToggle;
                PhoneLight.SetActive(lightToggle);
            }

            if (OVRInput.GetDown(OVRInput.Button.Two))
            {
                mapToggle = !mapToggle;
                PhoneMap.SetActive(mapToggle);
            }
        }
        else
        {
            PhoneLight.SetActive(false);
            PhoneMap.SetActive(false);
            phone.SetActive(true);
            phoneBatteryHolder.SetActive(false);
        }

        
	}
}
