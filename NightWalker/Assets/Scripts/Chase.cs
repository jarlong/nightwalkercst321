﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour {

    public GameObject player;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float angle = (Vector3.Angle(transform.position - player.transform.position, player.transform.forward));

        //move towards player when not in sight
        if (Vector3.Distance(transform.position, player.transform.position) > 5 && (angle < -70 || angle > 70))
        {
            transform.LookAt(player.transform.position);
            transform.Rotate(new Vector3(0, -90, 0), Space.Self);
            transform.Translate(new Vector3(2f * Time.deltaTime, 0, 0));
        }
    }
}
