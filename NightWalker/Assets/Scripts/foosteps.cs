﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class foosteps : MonoBehaviour {

    public AudioClip[] footstepSound;
    private CharacterController controller;
    private AudioSource source;
    private int footstepInterval = 0;

	// Use this for initialization
	void Start ()
    {
        controller = GetComponent<CharacterController>();
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(controller.isGrounded == true && controller.velocity.magnitude > 1f && source.isPlaying == false)
        {
            source.volume = Random.Range(0.2f, 0.4f);
            source.pitch = Random.Range(0.8f, 1.1f);
            source.Play();
        }
	}
}
