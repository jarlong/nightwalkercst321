﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playGuitar : MonoBehaviour {

    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            source.volume = Random.Range(0.4f, 0.6f);
            source.pitch = Random.Range(1.0f, 1.1f);
            source.Play();
        }
        
    }
}
