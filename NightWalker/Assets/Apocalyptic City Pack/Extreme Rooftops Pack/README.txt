﻿
===== Extreme Rooftops Pack =====


1. Intro
2. Content
3. How to use
4. Support

1. -=Intro=-
Thank you for purchasing the Extreme Rooftops Pack!
You are free to use the models in your own project in any way you like. 
We hope that you enjoy this amazing Rooftops pack.


2. -=Content=-
This package includes 84 game ready models.


3. -=How to use=-

=Placing=
Simply place the assets from Extreme Rooftops Pack\Models into your scene of choice. Al textures and materials are linked. You can apply a different shader or diffuse color, which you can edit in Extreme Rooftops Pack\Models\Model_....\Materials.


4. -=Support=-	
For more information or questions, please send your email to info@vertigo-games.com.


 Vertigo Games B.V.



